FROM ubuntu:16.04

MAINTAINER vsv10000ego@gmail.com

ENV CPU_CORE 4

ADD ./VOLUME_DATA /usr/local/VOLUME_DATA

RUN \
  apt-get update -qq && \
  apt-get install -y \
    git g++ cmake libsndfile-dev \
	sox ffmpeg

WORKDIR /usr/local/

RUN git clone https://Golubtsov_Vasiliy@bitbucket.org/Golubtsov_Vasiliy/autosyncvt.git

WORKDIR /usr/local/autosyncvt

